export const ANALYZE_TEXT = "ANALYZE_TEXT";
export const CLASSIFY_TEXT = "CLASSIFY_TEXT";
export const LOADING = "LOADING";
export const PERFORM_NER = "PERFORM_NER";
export const PERFORM_COREF = "PERFORM_COREF";
export const PERFORM_SRL = "PERFORM_SRL";