from os import listdir
from os.path import isfile, join
import json
import requests
import time

def run_pipeline(mypath,server_url,result_filepath):

    onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
    total = 0
    for f in onlyfiles:
        if(not f.endswith(".txt") ):
            continue
        
        time.sleep(5)
        
        print("Processing:" + f)

        file = open(mypath+'/'+f, "r")
        text = file.read()
        text = text.replace('\r', '').replace('\n', '')
        data = {'text':text}
        r = requests.post(url=server_url, json={'text': text}, headers={'Accept': 'application/json'})
        if(r.status_code == 404):
            continue
        response_json = r.json()
        file1 = open(result_filepath, "a+")
        file1.write(str(response_json['ratio']) + "\n")
        
        total = total + 1
        print("Total processed: " + str(total))


run_pipeline(
    'test_folder', 
    'http://127.0.0.1:5000/run-pipeline',
    'result_01.txt')
