from flask import Flask, request, jsonify
from flask_cors import CORS
import os

from .nlp import PharmaTextClassifier, NER, NeuralCoreference, SRL, LD

app = Flask(__name__)
CORS(app)

Ner = NER()
Coref = NeuralCoreference()
Srl = SRL()
Pharma_text_classifier = PharmaTextClassifier()
LD = LD()

def clear_text(text):
    text = text.replace("\r", "")
    text = text.replace("\n", "")
    return text

@app.route('/')
def index():
  return 'Pharma text analytics'


@app.route('/classify-text', methods=['POST'])
def classify_text():
    data = request.get_json()
    text = data['text']
    classification = Pharma_text_classifier.classify_text(clear_text(text))
    return classification


@app.route('/ner', methods=['POST'])
def ner_handler():
    data = request.get_json()
    text = data['text']
    entities = Ner.name_entities(clear_text(text))
    return jsonify(entities)


@app.route('/coreference', methods=['POST'])
def coreference_handler():
    data = request.get_json()
    text = data['text']
    entities = data['entities']
    coref_res = Coref.coref(clear_text(text), entities)
    return jsonify(coref_res)


@app.route('/srl', methods=['POST'])
def srl_handler():
    data = request.get_json()
    text = data['text']
    entities = data['entities']
    srl_labeled_text = Srl.label_text(clear_text(text), entities)
    return jsonify(srl_labeled_text)


@app.route('/run-pipeline', methods=['POST'])
def run_pipeline():
    try:
        data = request.get_json()
        text = data['text']
        classification = Pharma_text_classifier.classify_text(clear_text(text))
        ner = Ner.name_entities(clear_text(text))
        all_entities = ner['entities']['all_entities']

        map_iterator = map(lambda e: e['entity'], all_entities)
        all_entities = list(map_iterator)

        entities_p = ",".join(list(map(lambda e: e['entity'], list(filter(
            lambda x: x['type'] == "Pharma_org", ner['entities']['custom_entities'])))))
        entities_d = ",".join(list(map(lambda e: e['entity'], list(
            filter(lambda x: x['type'] == "Drug", ner['entities']['custom_entities'])))))

        coref_res = Coref.coref(clear_text(text), all_entities)
        text_with_coref_resolved = coref_res['sentence']
        srl_labeled_text = Srl.label_text(
            clear_text(text_with_coref_resolved), all_entities)

        filtered_tripples = srl_labeled_text['tripples_filtered']

        annotated = LD.annotate_text(
            text, entities_p, entities_d, filtered_tripples)

        return {
            'classification': classification,
            'ner': ner,
            'srl': srl_labeled_text,
            'ratio': annotated['ratio']
        }
    except:
        flask.abort(404)

@app.route('/test-annotate', methods=['GET'])
def test_annotate():
    data = request.get_json()
    text = data['text']
    entities_p = data['entities_p']
    entities_d = data['entities_d']

    res = LD.annotate_text(text, entities_p, entities_d, None)
    return res['res']

@app.route('/resources', methods=['POST'])
def likely_resources_handler():
    data = request.get_json()
    word = data['word']
    res = LD.get_likely_resources(word)
    return jsonify(res)

def main():
    app.run(debug=False, use_reloader=False, host="127.0.0.1")

if __name__ == '__main__':
    main()
