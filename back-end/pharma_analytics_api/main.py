from nlp import PharmaTextClassifier, NER, NeuralCoreference, SRL

contents = """
Sanofi has invested more than $5.51 billion on production over the last six or seven years, much of it on pilot projects to prepare 
its manufacturing operations to produce drugs like Dupixent, its injected treatment for treating asthma and eczema that it developed with Regeneron. 
But more new drugs are coming as it sharpens its development focus. It recently dumped more than 465 jobs in a reorganization of its R&D and given 
up some partnerships, including Regeneron, as it moves away from cardiovascular diseases and toward immuno-oncology drugs and gene therapies.
"""

demo_text_1 = "Pfizer, the popular drugmaker has released a new treatment labeled Nova!"


def test_pharma_text_classifier():
   demo_text_1 = "This is a demo classification text"
   demo_text_2 = "Novartis has released a new prescription drug known as Nova!"
   pharma_text_classifier = PharmaTextClassifier()
   res1 = pharma_text_classifier.classify_text(demo_text_1)
   res2 = pharma_text_classifier.classify_text(demo_text_2)

   print(res1)
   print(res2)


def test_entity_recognizers():
   ner = NER()
   entities = ner.name_entities(contents)

def test_coreference():
   coref = NeuralCoreference()
   new_text = coref.coref(contents, ['Daiichi', 'Roche', 'Pfizer'])
   return new_text

def test_srl(text):
   srl = SRL()
   srl.label_text(text, ['Daiichi', 'Roche', 'Pfizer'])

if __name__ == "__main__":
   test_pharma_text_classifier()
   # test_entity_recognizers()
   # new_text = test_coreference()
   # test_srl(new_text)

