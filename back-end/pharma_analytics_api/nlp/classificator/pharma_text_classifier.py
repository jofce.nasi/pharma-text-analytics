from .text_classifier import TextClassifier
from ..utils.enums.classificator import Classificator

import os.path
import ktrain

class PharmaTextClassifier(TextClassifier):

    __bert_predictor_url = "static/classification-models/bert/bert_predictor"
    __xlnet_model_url = "url"

    def __init__(self):
        self.__init_predictors()

    def __init_predictors(self):
        self._bert_predictor = ktrain.load_predictor(self.__bert_predictor_url)
        self._bert_predictor.model._make_predict_function()


    def classify_text(self, text: str, classificator=Classificator.bert):
        prediction = {
            "predicted_class": None
        }
        if(classificator == Classificator.bert):
            predicted = self._bert_predictor.predict(
                [text], 
                return_proba=True)
            prediction['predicted_class'] = "No Pharma" if predicted[0][0] > predicted[0][1] else "Pharma"
        elif(classificator == Classificator.xlnet):
            return ""
        
        return prediction

if __name__ == "__main__":
    demo_text = "This is a demo classification text"
    pharma_text_classifier = PharmaTextClassifier()
    res = pharma_text_classifier.classify_text(demo_text)
    print(res)
