from abc import ABC, abstractmethod

from ..utils.enums.classificator import Classificator

class TextClassifier(ABC):
    
    def __init__(self):
        print('==Init Classifier==')

    @abstractmethod
    def classify_text(self, text, classificator=Classificator.bert):
        """
            Task specific, should be implemented by the concrete component that inherrits the class
        """
        raise NotImplementedError


if __name__ == "__main__":
   print("Implement concrete classifier")
   
