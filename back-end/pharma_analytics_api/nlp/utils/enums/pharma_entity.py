import enum

class PharmaEntity(enum.Enum):
    pharma_org = 1
    drug = 2
