from typing import Dict, List
import spacy
import numpy as np
import torch
from pytorch_pretrained_bert.modeling import BertConfig

from .data_load import HParams
from .new_model import Net
from ....utils import PharmaEntity
from ..abstract_recognizer import AbstractRecognizer


class BioBERTRecognizer(AbstractRecognizer):

    def __init__(self, models_and_entities: Dict[str, List[PharmaEntity]] = {'': []}):
        super().__init__(models_and_entities)
        print("==Init Bio BERT NER==")
        self.__init_model()

    def __init_model(self):
        self.__config = BertConfig(
            vocab_size_or_config_json_file="static/entities/bio-bert/bert_config.json")
        self.__model_1 = self.__build_model(
            self.__config, "static/entities/bio-bert/bio_bert_ner_checkpoint.pt", HParams('bc5cdr'))
        self.__model_2 = self.__build_model(
            self.__config, "static/entities/bio-bert/bio_bert_ner_checkpoint_13CG.pt", HParams('bionlp3g'))


    def __build_model(self, config, state_dict, hp):
        model = Net(config, vocab_len=len(hp.VOCAB), bert_state_dict=None)
        _ = model.load_state_dict(torch.load(state_dict, map_location='cpu'))
        _ = model.to('cpu')
        return model

    def __process_query(self, query, hp, model):
        s = query
        split_s = ["[CLS]"] + s.split()+["[SEP]"]
        x = []  # list of ids
        is_heads = []  # list. 1: the token is the first piece of a word

        for w in split_s:
            tokens = hp.tokenizer.tokenize(
                w) if w not in ("[CLS]", "[SEP]") else [w]
            xx = hp.tokenizer.convert_tokens_to_ids(tokens)
            is_head = [1] + [0]*(len(tokens) - 1)
            x.extend(xx)
            is_heads.extend(is_head)

        x = torch.LongTensor(x).unsqueeze(dim=0)

        # Process query
        model.eval()
        _, _, y_pred = model(x, torch.Tensor([1, 2, 3]))  # just a dummy y value
        # Get prediction where head is 1
        preds = y_pred[0].cpu().numpy()[np.array(is_heads) == 1]

        # convert to real tags and remove <SEP> and <CLS>  tokens labels
        preds = [hp.idx2tag[i] for i in preds][1:-1]
        final_output = []
        for word, label in zip(s.split(), preds):
            final_output.append([word, label])
        return final_output

    def recognize_entities(self, sentence: str, entities_subset=[]):

        hp_1 = HParams('bc5cdr')
        out_bc5cdr = self.__process_query(query=sentence, hp=hp_1, model=self.__model_1)

        hp_2 = HParams('bionlp3g')
        out_bionlp3g = self.__process_query(query=sentence, hp=hp_2, model=self.__model_2)
        
        list1_bc5cdr, list2_bc5cdr = zip(*out_bc5cdr)
        list1_bionlp3g, list2_bionlp3g = zip(*out_bionlp3g)

        predictions = {
            'tokens': list1_bc5cdr,
            'predictions_bio_bc5cdr': list2_bc5cdr,
            'predictions_bio_bionlp3g': list2_bionlp3g,
        }

        
        return predictions
