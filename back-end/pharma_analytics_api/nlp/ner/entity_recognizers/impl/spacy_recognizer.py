from typing import Dict, List
import spacy

from ....utils import PharmaEntity
from ..abstract_recognizer import AbstractRecognizer

class SpacyRecognizer(AbstractRecognizer):

    def __init__(self, models_and_entities: Dict[str, List[PharmaEntity]] = {'': []}):
        super().__init__(models_and_entities)
        self.__init_predictors()

    def __init_predictors(self):
        self.__predictors = {}

        for model_path in self._models_and_entites:
            nlp = spacy.load(model_path)
            self.__predictors[model_path] = nlp

    def recognize_entities(self, sentence: str, entities_subset=[]):

        if(len(entities_subset) == 0):
            return None

        predictions = {
            'tokens': [],
            'predictions_bio': []
        }

        for p in self.__predictors:
            l = list(set(self._models_and_entites[p]) & set(entities_subset))

            if(len(l) == 0):
                continue

            doc = self.__predictors[p](self._clean_text(sentence))

            predictions['tokens'] = [t.text for t in doc]
            predictions['predictions_bio'].append(
                [t.ent_iob_ if t.ent_iob_ == "O" else t.ent_iob_ + "-" + t.ent_type_ for t in doc])

        return predictions
