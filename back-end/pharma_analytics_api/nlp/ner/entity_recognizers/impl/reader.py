from allennlp.data import Instance
from allennlp.data.token_indexers import TokenIndexer
from allennlp.data.token_indexers.single_id_token_indexer import SingleIdTokenIndexer
from allennlp.data.tokenizers import Token
from allennlp.data.tokenizers import Tokenizer
from allennlp.data.tokenizers.word_tokenizer import WordTokenizer
from allennlp.nn import util as nn_util
from allennlp.data.fields import TextField, MetadataField, ArrayField, SequenceLabelField
from allennlp.data.vocabulary import Vocabulary
from allennlp.data.dataset_readers import DatasetReader
from allennlp.common.checks import ConfigurationError
from allennlp.data.dataset_readers.dataset_utils import to_bioul

from typing import Dict, List, Sequence, Iterable
import json
import logging

from overrides import overrides

import tqdm
import itertools

logger = logging.getLogger(__name__)  # pylint: disable=invalid-name

def _is_divider(line: str) -> bool:
    empty_line = line.strip() == ''
    if empty_line:
        return True
    else:
        first_token = line.split()[0]
        if first_token == "-DOCSTART-":  # pylint: disable=simplifiable-if-statement
            return True
        else:
            return False


@DatasetReader.register("pharmareader")
class PhDatasetReader(DatasetReader):
    """
    Parameters
    ----------
    tokenizer : ``Tokenizer``, optional
        Tokenizer to use to split the title and abstrct into words or other kinds of tokens.
        Defaults to ``WordTokenizer()``.
    token_indexers : ``Dict[str, TokenIndexer]``, optional
        Indexers used to define input token representations. Defaults to ``{"tokens":
        SingleIdTokenIndexer()}``.
    """


    def __init__(self,
                tokenizer: Tokenizer = None,
                token_indexers: Dict[str, TokenIndexer] = None,
                lazy: bool = False) -> None:
        super().__init__(lazy=lazy)

        self._tokenizer = tokenizer or WordTokenizer()
        self._token_indexers = token_indexers or {"tokens": SingleIdTokenIndexer()}
        self._original_coding_scheme = "IOB1"

    @overrides
    def _read(self, file_path):
        with open(file_path, "r") as data_file:
          # Group into alternative divider / sentence chunks.
          for is_divider, lines in itertools.groupby(data_file, _is_divider):
              # Ignore the divider chunks, so that `lines` corresponds to the words
              # of a single sentence.
              if not is_divider:
                  fields = [line.strip().split() for line in lines]
                  # unzipping trick returns tuples, but our Fields need lists
                  fields = [list(field) for field in zip(*fields)]
                  tokens_, ner_tags = fields
                  # TextField requires ``Token`` objects
                  tokens = [Token(token) for token in tokens_]

                  yield self.text_to_instance(tokens, ner_tags)

    @overrides
    def text_to_instance(self, tokens: List[Token],
                         ner_tags: List[str] = None) -> Instance:  # type: ignore
        # pylint: disable=arguments-differ
        sequence = TextField(tokens, self._token_indexers)
        instance_fields: Dict[str, Field] = {'tokens': sequence}
        instance_fields["metadata"] = MetadataField(
            {"words": [x.text for x in tokens]})

        coded_ner = ner_tags
        
        if(coded_ner == None):
            coded_ner = ["O" for x in sequence]
        
        instance_fields['ner_tags'] = SequenceLabelField(
            coded_ner, sequence, "ner_tags")
        instance_fields['tags'] = SequenceLabelField(
            coded_ner, sequence, "labels")
        return Instance(instance_fields)
