from abc import ABC, abstractmethod
from typing import Dict, List

from ...utils import PharmaEntity

class AbstractRecognizer(ABC):

    def __init__(self, models_and_entities: Dict[str, List[PharmaEntity]] = {'': []}):
        print("== Init abstract recognizer ==")
        self._models_and_entites = models_and_entities

    @abstractmethod
    def recognize_entities(self, text, entities_subset = []):
        """
            TODO: Standardize output
            Task specific, should be implemented by the concrete component that inherrits the class
        """
        raise NotImplementedError

    def _clean_text(self, text):
        return text.replace('\r', '').replace('\n', '')

if __name__ == "__main__":
   print("Implement concrete recognizer")
