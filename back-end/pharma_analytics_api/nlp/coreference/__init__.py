from .neural_coreference import NeuralCoreference

__all__ = [
    'NeuralCoreference'
]