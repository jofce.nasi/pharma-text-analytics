import neuralcoref
import spacy
import pandas as pd
from typing import List

class NeuralCoreference():

    __path_to_drugs_entities = "static/entities/drugs/drug_entities.csv"
    __path_to_ph_org_entities = "static/entities/pharmaceutical-organizations/ph_entities.csv"
    __pipe = "neuralcoref"

    __pharma_org_known_tags = ['pharmaceutical', 'organization', 'company', 'drugmaker', 'pharma', 'pharma company']
    __drug_known_tags = ['drug', 'treatment', 'therapy']


    def __init__(self):
        print("==Init Neural Coref==")
        self.__init_spacy_nlp()
        self.__init_custom_conversion_dictionary()

    def __init_spacy_nlp(self):
        self.__nlp = spacy.load('en')

        coref = neuralcoref.NeuralCoref(self.__nlp.vocab, greedyness=0.55)
        self.__nlp.add_pipe(coref, name=self.__pipe)

    def __init_custom_conversion_dictionary(self):
        drugs = pd.read_csv(self.__path_to_drugs_entities)
        ph_orgs = pd.read_csv(self.__path_to_ph_org_entities)

        dict_pharma_terms = {}
        
        # Build conversion dict
        for d in drugs['Text'][0:2000]:
            dict_pharma_terms[d] = self.__drug_known_tags
        
        for p in ph_orgs[ph_orgs.Text.str.find(' ') == -1]['Text'][0:2000]:
            dict_pharma_terms[p] = self.__pharma_org_known_tags
            
        # Add dict to neuralcoref pipe
        self.__nlp.get_pipe(self.__pipe).set_conv_dict(dict_pharma_terms)

    def __replace_in_string(self, original_text, text_span_for_replacement, start_pos, end_pos):
        new_text = original_text[0:start_pos]
        new_text = new_text + text_span_for_replacement
        new_text = new_text + original_text[end_pos:]
        return new_text


    def __replace_entities_mentions(self, doc, clusters, entities):
        resolved = list(tok.text_with_ws for tok in doc)
        for cluster in clusters:
            checkSet = set([True if (e.lower() in cluster.main.text.lower() or cluster.main.text.lower() in e.lower()) else False for e in entities])
            if(True in checkSet):
                for coref in cluster:
                    if coref != cluster.main:
                        resolved[coref.start] = cluster.main.text + \
                            doc[coref.end-1].whitespace_
                        for i in range(coref.start+1, coref.end):
                            resolved[i] = ""
        return ''.join(resolved)

    def coref(self, text: str, entities: List = []):
        # TODO: Check clusters, replace correferences & replace in text
        # Return text with replaced entities

        doc = self.__nlp(text)
        return {
            "sentence": self.__replace_entities_mentions(doc, doc._.coref_clusters, entities)
        }
