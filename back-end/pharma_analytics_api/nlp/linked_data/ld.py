from nltk.corpus import wordnet
import pandas as pd
import re
import textdistance
import requests
import json

from .rdf_builder import RDFBuilder

class LD():

    __default_schema_org_dataset = "static/ld/schema-org-classes-and-properties-2.csv"
    __cosine_similarity_threshold = 0.9

    __SPOTLIGHT_URL = "https://api.dbpedia-spotlight.org/en"
    __SPOTLIGHT_URL_LOCAL = "http://localhost:2222/"

    def __init__(self):
        print("== Init LD ==")
        self.__rdf_builder = RDFBuilder()

    def __get_word_synonyms(self, word):
        synonyms = []
        for syn in wordnet.synsets(word):
            for l in syn.lemmas():
                synonyms.append(l.name())

        return synonyms

    def __get_related_nouns_from_verb(self, verb):
        set_of_related_nouns = set()

        for lemma in wordnet.lemmas(wordnet.morphy(verb, wordnet.VERB), pos="v"):
            for related_form in lemma.derivationally_related_forms():
                for synset in wordnet.synsets(related_form.name(), pos=wordnet.NOUN):
                    if wordnet.synset('person.n.01') in synset.closure(lambda s: s.hypernyms()):
                        for l in synset.lemmas():
                            set_of_related_nouns.add(l.name())
                        
        return set_of_related_nouns

    def __camel_case_split(self, identifier):
        matches = re.finditer('.+?(?:(?<=[a-z])(?=[A-Z])|(?<=[A-Z])(?=[A-Z][a-z])|$)', identifier)
        return " ".join([m.group(0).lower() for m in matches])

    def __check_synonims_in_labels(self, label, text_to_match):
        synonyms = self.__get_word_synonyms(text_to_match)
        max_td = 0
        for synonym in synonyms:
            td = textdistance.cosine.similarity(label.lower(), synonym.lower())
            if(td > max_td):
                max_td = td

        return max_td

    def get_likely_resources(self, word):
        df = pd.read_csv(self.__default_schema_org_dataset)

        df.fillna("", inplace=True)

        df['cosine'] = df['label'].apply(
            lambda x: self.__check_synonims_in_labels(x, word))

        res = df[df['cosine'] > self.__cosine_similarity_threshold]

        return {
            "entities": [e for e in res['entity']],
            "domains": [e for e in res['domains']],
            "ranges": [e for e in res['ranges']],
            "similarities": [e for e in res['cosine']]
        }

    def __get_resources_from_json_annotation(self, resources):
        filtered_res = list(map(lambda e: {
            'uri': e['@URI'], 
            'entity': e['@surfaceForm'],
            'types': list(filter(lambda x: 'DBpedia' in x or 'Schema' in x, e['@types'].split(',')))
            }, 
        resources))

        return filtered_res

    def __handle_tripples(self, tripples):
        
        for tripple in tripples:
            related = self.__get_related_nouns_from_verb(tripple['verb_root'])
            print('=======')
            print("verb: {} and noun(s): {}".format(tripple['verb_root'], related))
            for r in related:
                print(self.get_likely_resources(r))

        return ''

    def annotate_text(self, text, entities_p, entities_d, filtered_tripples):
        if(len(text) > 2000):
            text = text[:1999]
        
        response = requests.get(
            self.__SPOTLIGHT_URL + '/annotate',
            params={'text': text},
            headers={'Accept': 'application/json'})

        print(response.status_code)
        
        response_json = response.json()

        resources = self.__get_resources_from_json_annotation(response_json['Resources'])
        initial_rdf = self.__rdf_builder.build_rdf_from_resources(resources, entities_p, entities_d)

        return initial_rdf

