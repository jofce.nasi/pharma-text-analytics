from rdflib import Graph

class RDFBuilder():

    __SCHEMA_ORG_URL = "http://schema.org/"
    __DBPEDIA_URL = "http://dbpedia.org/resource/"

    __OWN_ONTOLOGY_URL = "http://ph-analytics.org/resource/"

    __intermediate_result_output_file = "static/intermediate.txt"

    def __init__(self):
         print("== Init RDF Builder ==")

    def build_rdf_from_resources(self, resources, entities_p, entities_d):

        res_turtle = """@prefix dbpedia: <http://dbpedia.org/resource/> . \n@prefix dbpo: <http://dbpedia.org/ontology/> . \n@prefix owl: <http://www.w3.org/2002/07/owl#> . \n@prefix schema: <http://schema.org/> . \n\n"""

        res_uris = [x['uri'] for x in resources]

        seen = set()
        unique = [obj for obj in resources if obj['uri'] not in seen and not seen.add(obj['uri'])]

        distinct_resources = list()

        for s in seen:
            find = [x for x in resources if x['uri'] == s]
            distinct_resources.append(find[0])

        total = 0
        added = 0

        for res in distinct_resources:
            entity_from_uri = res['uri'][res['uri'].rindex('/') + 1:]
            if(len(res['types']) > 0):
                total += 1
                for t in res['types']:
                    if("Schema" in t):
                        res_turtle += "dbpedia:" + entity_from_uri + " a schema:" + t[7:] + " .\n"
                    elif("DBpedia" in t):
                        res_turtle += "dbpedia:" + entity_from_uri + " a dbpo:" + t[8:] + " .\n"

        res_turtle += "\n"

        for e in entities_p.split(","):
            if(len(e) == 0):
                continue
            added += 1
            res_turtle += "dbpedia:" + e + " a schema:MedicalOrganization .\n"

        for e in entities_d.split(","):
            if(len(e) == 0):
                continue
            added +=2
            res_turtle += "dbpedia:" + e + " a schema:Drug .\n"
            res_turtle += "dbpedia:" + e + " a dbpo:Drug .\n"

        self.append_result_to_file(res_turtle, entities_p, entities_d)
        
        return {
            "res": res_turtle, 
            "ratio": added / (total + added)
        }

    def append_result_to_file(self, res_turtle, entities_p, entities_d):
        result_file = open(self.__intermediate_result_output_file, "a+")
        result_file.write("\n")
        result_file.write("=======FILE========")
        result_file.write("\n")
        result_file.write("Pharma Entities: " + str(entities_p) + "\n")
        result_file.write("Drug Entities: " + str(entities_d) + "\n")
        result_file.write("\n")
        result_file.write(res_turtle + "\n")
        result_file.write("\n")
        result_file.close()

    def handle_srl_tripples(self, resources, initial_rdf, filtered_tripples):
        
        for t in filtered_tripples:
            initial_rdf += "ph-analytics:" + t['agent'] + " ph-analytics:" + t['verb_root'] + " ph-analytics:" + t['patient']  + " \n"

        return initial_rdf
