from nltk.tokenize import sent_tokenize
from allennlp.predictors.predictor import Predictor

import re
from nltk.stem import WordNetLemmatizer

class SRL():

    __allen_srl_predictor_path = "https://s3-us-west-2.amazonaws.com/allennlp/models/bert-base-srl-2019.06.17.tar.gz"

    def __init__(self):
        print("==Init Allen SRL==")
        self.__lemmatizer = WordNetLemmatizer()
        self.__init_predictor()

    def __init_predictor(self):
        self.srl_predictor = Predictor.from_path(self.__allen_srl_predictor_path)

    def __sentence_tokenize(self, text):
        return sent_tokenize(text)
    
    def __is_any_entity_in_sentence(self, sentence, entities) -> bool:
        checkSet = set([True if e in sentence else False for e in entities])
        if(True in checkSet):
            return True
        return False

    def label_text(self, text, entities):
        # Sentence tokenize
        # Filter sentences that contain any of the entities of interest
        # Apply SRL to each sentence
        # Return SRL for each verb for each sentence
        sentences = self.__sentence_tokenize(text)
        filtered_sentences = [s for s in sentences if self.__is_any_entity_in_sentence(s, entities) ]
        
        sentences_srl = []

        index = 0
        for s in filtered_sentences:
            predicted = self.srl_predictor.predict(
                sentence=s
            )

            sent_srl = {
                "index": index,
                "sentence": s,
                "verbs": [],
                "tripples": []
            }

            for v in predicted['verbs']:
                agent = re.search('\[ARG0([^]]*)\]', v['description'])
                patient = re.search('\[ARG1([^]]*)\]', v['description'])

                if(agent):
                    agent = agent.group()[6:-1].strip()
                else:
                    agent = ""
                if(patient):
                    patient = patient.group()[6:-1].strip()
                else:
                    patient = ""

                if(v['description'].find('ARG') != -1):
                    sent_srl["verbs"].append({
                        "verb": v['verb'],
                        "description": v['description']
                    })
                    if(agent != "" and patient != ""):
                        sent_srl["tripples"].append({
                            "agent": agent,
                            "verb_root": self.__lemmatizer.lemmatize(v['verb'], pos='v'),
                            "patient": patient
                        })
                    

            sentences_srl.append(sent_srl)
            index = index + 1

        res_map = list(map(lambda x: x['tripples'], sentences_srl))
        res_map = [item for sublist in res_map for item in sublist]

        res_map_verbs = list(map(lambda x: x['verbs'], sentences_srl))
        res_map_verbs = [item['verb'] for sublist in res_map_verbs for item in sublist]

        # res_map = list(filter(lambda x: not any([True if (' ' + v + ' ') in x['patient'] else False for v in res_map_verbs]), res_map))
        

        return {
            # 'tripples': res_map,
            'tripples_default': res_map,
            'tripples_filtered': list(filter(lambda x: not any([True if (' ' + v + ' ') in x['patient'] else False for v in res_map_verbs]), res_map)),
            'sentences_srl': sentences_srl
        }
