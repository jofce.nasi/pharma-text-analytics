from setuptools import setup

setup(
    name='pharma_analytics_api',
    packages=['pharma_analytics_api'],
    include_package_data=True,
    install_requires=[
        'flask',
    ],
)
