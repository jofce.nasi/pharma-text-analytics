# PharmKE: Knowledge ExtractionPlatform for Pharmaceutical Texts using Transfer Learning

## Steps to run the pipeline

1. Download the models from: https://finkiukim-my.sharepoint.com/:f:/g/personal/nasi_jofche_finki_ukim_mk/EiOQMvsXYDBIkDBREFA70TYBNaZp6_0s8ZO2jpc1h-9Apg?e=tD8qnM

2. Insert the models from `bert` folder into `back-end/static/classification-models`
3. Replace the models from `ner_models.zip` into `back-end/static/ner-models`
4. Install the requirements from `requirements.txt`
5. The text files urls are available in the `news_u.csv` file to be scraped. Once the texts are scraped (only the first 1500), run the `script.py` to reproduce the results

## References

The following libraries were used in this research: 

```bibtex
@inproceedings{Gardner2017AllenNLP,
  title={AllenNLP: A Deep Semantic Natural Language Processing Platform},
  author={Matt Gardner and Joel Grus and Mark Neumann and Oyvind Tafjord
    and Pradeep Dasigi and Nelson F. Liu and Matthew Peters and
    Michael Schmitz and Luke S. Zettlemoyer},
  year={2017},
  Eprint = {arXiv:1803.07640},
}
```

```
@article{10.1093/bioinformatics/btz682,
    author = {Lee, Jinhyuk and Yoon, Wonjin and Kim, Sungdong and Kim, Donghyeon and Kim, Sunkyu and So, Chan Ho and Kang, Jaewoo},
    title = "{BioBERT: a pre-trained biomedical language representation model for biomedical text mining}",
    journal = {Bioinformatics},
    year = {2019},
    month = {09},
    issn = {1367-4803},
    doi = {10.1093/bioinformatics/btz682},
    url = {https://doi.org/10.1093/bioinformatics/btz682},
}
```